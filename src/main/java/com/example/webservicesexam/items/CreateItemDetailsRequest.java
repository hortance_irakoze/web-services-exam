package com.example.webservicesexam.items;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
        "itemDetails"
})
@XmlRootElement(name="CreateItemDetailsRequest")
public class CreateItemDetailsRequest {
    @XmlElement(required=true)
    protected ItemDetails itemDetails;

    public ItemDetails getItemDetails() {
        return itemDetails;
    }
    public void setItemDetails(ItemDetails value){
        this.itemDetails=value;
    }
}
