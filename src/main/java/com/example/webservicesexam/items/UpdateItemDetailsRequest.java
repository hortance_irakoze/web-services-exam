package com.example.webservicesexam.items;
import com.example.webservicesexam.soap.bean.Item;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
        "itemDetails"
})
@XmlRootElement(name = "UpdateItemDetailsRequest")
public class UpdateItemDetailsRequest {
    @XmlElement(required = true)
    protected ItemDetails itemDetails;


    public ItemDetails getItemDetails() {
        return itemDetails;
    }


    public void setStudentDetails(ItemDetails value) {
        this.itemDetails = value;
    }
}
