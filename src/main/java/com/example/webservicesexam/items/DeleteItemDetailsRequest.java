package com.example.webservicesexam.items;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
        "id"
})
@XmlRootElement(name="DeleteItemDetailsRequest")
public class DeleteItemDetailsRequest {
    protected long id;
    public long getId(){
        return id;
    }
    public void  setId(long value){
        this.id=value;
    }
}
