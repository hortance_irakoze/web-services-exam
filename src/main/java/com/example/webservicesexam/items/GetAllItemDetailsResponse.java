package com.example.webservicesexam.items;
import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
        "itemDetails"
})
@XmlRootElement(name = "GetAllItemDetailsResponse")
public class GetAllItemDetailsResponse {
    @XmlElement(required = true)
    protected List<ItemDetails> itemDetails;
    public List<ItemDetails> getItemDetails() {
        if (itemDetails == null) {
            itemDetails = new ArrayList<ItemDetails>();
        }
        return this.itemDetails;
    }
}
