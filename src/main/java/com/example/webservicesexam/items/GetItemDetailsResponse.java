package com.example.webservicesexam.items;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
        "itemDetails"
})
@XmlRootElement(name = "GetItemDetailsResponse")
public class GetItemDetailsResponse {
    @XmlElement(required = true)
    protected ItemDetails itemDetails;

    /**
     * Gets the value of the ItemDetails property.
     *
     * @return
     *     possible object is
     *     {@link ItemDetails }
     *
     */
    public ItemDetails getItemDetails() {
        return itemDetails;
    }

    /**
     * Sets the value of the ItemDetails property.
     *
     * @param value
     *     allowed object is
     *     {@link ItemDetails }
     *
     */
    public void setItemDetails(ItemDetails value) {
        this.itemDetails = value;
    }

}
