package com.example.webservicesexam.items;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


    
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
            "itemDetails",
            "message"
    })
    @XmlRootElement(name = "UpdateitemDetailsResponse")
    public class UpdateItemDetailsResponse {

        @XmlElement(required = true)
        protected ItemDetails itemDetails;
        @XmlElement(required = true)
        protected String message;


        public ItemDetails getitemDetails() {
            return itemDetails;
        }

        public void setItemDetails(ItemDetails value) {
            this.itemDetails = value;
        }


        public String getMessage() {
            return message;
        }

        public void setMessage(String value) {
            this.message = value;
        }

    

}
