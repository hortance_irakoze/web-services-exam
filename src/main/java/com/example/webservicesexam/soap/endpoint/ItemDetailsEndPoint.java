package com.example.webservicesexam.soap.endpoint;

import com.example.webservicesexam.items.*;
import com.example.webservicesexam.soap.bean.Item;
import com.example.webservicesexam.soap.repository.ItemsRepository;
import com.example.webservicesexam.items.CreateItemDetailsResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ws.server.endpoint.annotation.Endpoint;
import org.springframework.ws.server.endpoint.annotation.PayloadRoot;
import org.springframework.ws.server.endpoint.annotation.RequestPayload;
import org.springframework.ws.server.endpoint.annotation.ResponsePayload;

import java.util.List;
import java.util.Optional;

@Endpoint
public class ItemDetailsEndPoint {
    @Autowired
    private ItemsRepository itemsRepository;
    @PayloadRoot(namespace = "http://webservicesexam/items",localPart = "GetItemDetailsRequest")
    @ResponsePayload
    public GetItemDetailsResponse findById(@RequestPayload GetItemDetailsRequest request){
        Item item=itemsRepository.findById((int) request.getId()).get();
        GetItemDetailsResponse itemDetailsResponse=mapItemDetails(item);
        return itemDetailsResponse;
    }
    @PayloadRoot(namespace = "http://webservicesexam/items", localPart = "GetAllItemDetailsRequest")
    @ResponsePayload
    public GetAllItemDetailsResponse findAll(@RequestPayload GetAllItemDetailsResponse request){
        GetAllItemDetailsResponse allItemDetailsResponse = new GetAllItemDetailsResponse();

        List<Item> items = itemsRepository.findAll();
        for (Item item: items){
            GetItemDetailsResponse itemDetailsResponse = mapItemDetails(item);
            allItemDetailsResponse.getItemDetails().add(itemDetailsResponse.getItemDetails());
        }
        return allItemDetailsResponse;
    }
    @PayloadRoot(namespace = "http://webservicesexam/items", localPart = "CreateItemDetailsRequest")
    @ResponsePayload
    public CreateItemDetailsResponse save(@RequestPayload CreateItemDetailsRequest request) {
        itemsRepository.save(new Item(request.getItemDetails().getId(),
                request.getItemDetails().getItemCode(),
                request.getItemDetails().getName(),
                request.getItemDetails().getPrice(),
                request.getItemDetails().getStatus(),
                request.getItemDetails().getSupplier()
        ));

        CreateItemDetailsResponse itemDetailsResponse = new CreateItemDetailsResponse();
        itemDetailsResponse.setItemDetails(request.getItemDetails());
        itemDetailsResponse.setMessage("Created Successfully");
        return itemDetailsResponse;
    }

    @PayloadRoot(namespace = "http://webservicesexam/items", localPart = "UpdateSupplierDetailsRequest")
    @ResponsePayload
    public UpdateItemDetailsResponse update(@RequestPayload UpdateItemDetailsRequest request) {
        UpdateItemDetailsResponse itemDetailsResponse = null;
        Optional<Item> existingItem = this.itemsRepository.findById((int) request.getItemDetails().getId());
        if(existingItem.isEmpty() || existingItem == null) {
            itemDetailsResponse = mapItemDetail(null, "Id not found");
        }
        if(existingItem.isPresent()) {

            Item _item = existingItem.get();
            _item.setName(request.getItemDetails().getName());
            _item.setItemCode(request.getItemDetails().getItemCode());
            _item.setPrice(request.getItemDetails().getPrice());
            _item.setStatus(request.getItemDetails().getStatus());
            _item.setSupplier(request.getItemDetails().getSupplier());
            itemsRepository.save(_item);
            itemDetailsResponse = mapItemDetail(_item, "Updated successfully");

        }
        return itemDetailsResponse;
    }

    @PayloadRoot(namespace = "http://webservicesexam/items", localPart = "DeleteItemDetailsRequest")
    @ResponsePayload
    public DeleteItemDetailsResponse delete(@RequestPayload DeleteItemDetailsRequest request) {

        System.out.println("ID: "+request.getId());
        itemsRepository.deleteById((int) request.getId());

        DeleteItemDetailsResponse itemDetailsResponse = new DeleteItemDetailsResponse();
        itemDetailsResponse.setMessage("Deleted Successfully");
        return itemDetailsResponse;
    }

    private GetItemDetailsResponse mapItemDetails(Item item){
        ItemDetails itemDetails = mapItem(item);

        GetItemDetailsResponse itemDetailsResponse = new GetItemDetailsResponse();

        itemDetailsResponse.setItemDetails(itemDetails);
        return itemDetailsResponse;
    }

    private UpdateItemDetailsResponse mapItemDetail(Item item, String message) {
        ItemDetails itemDetails = mapItem(item);
        UpdateItemDetailsResponse itemDetailsResponse = new UpdateItemDetailsResponse();

        itemDetailsResponse.setItemDetails(itemDetails);
        itemDetailsResponse.setMessage(message);
        return itemDetailsResponse;
    }
    private ItemDetails mapItem(Item item){
        ItemDetails itemDetails = new ItemDetails();
        itemDetails.setItemCode(item.getItemCode());
        itemDetails.setId(item.getId());
        itemDetails.setName(item.getName());
        itemDetails.setPrice(item.getPrice());
        itemDetails.setStatus(item.getStatus());
        itemDetails.setSupplier(item.getSupplier());
        return itemDetails;
    }
}
