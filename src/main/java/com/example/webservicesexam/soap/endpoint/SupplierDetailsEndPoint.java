package com.example.webservicesexam.soap.endpoint;

import com.example.webservicesexam.items.*;
import com.example.webservicesexam.soap.bean.Item;
import com.example.webservicesexam.soap.bean.Supplier;
import com.example.webservicesexam.soap.repository.ISuppliersRepository;
import com.example.webservicesexam.items.CreateItemDetailsResponse;
import com.example.webservicesexam.suppliers.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ws.server.endpoint.annotation.Endpoint;
import org.springframework.ws.server.endpoint.annotation.PayloadRoot;
import org.springframework.ws.server.endpoint.annotation.RequestPayload;
import org.springframework.ws.server.endpoint.annotation.ResponsePayload;

import java.util.List;
import java.util.Optional;

@Endpoint
public class SupplierDetailsEndPoint {
    @Autowired
    private ISuppliersRepository suppliersRepository;
    @PayloadRoot(namespace = "http://webservicesexam/suppliers",localPart = "GetSupplierDetailsRequest")
    @ResponsePayload
    public GetSupplierDetailsResponse findById(@RequestPayload GetItemDetailsRequest request){
        Supplier supplier=suppliersRepository.findById((int) request.getId()).get();
        GetSupplierDetailsResponse supplierDetailsResponse=mapSupplierDetails(supplier);
        return supplierDetailsResponse;
    }
    @PayloadRoot(namespace = "http://webservicesexam/items", localPart = "GetAllSupplierDetailsRequest")
    @ResponsePayload
    public GetAllSupplierDetailsResponse findAll(@RequestPayload GetAllSupplierDetailsRequest request){
        GetAllSupplierDetailsResponse allSupplierDetailsResponse = new GetAllSupplierDetailsResponse();

        List<Supplier> suppliers = suppliersRepository.findAll();
        for (Supplier supplier: suppliers){
            GetSupplierDetailsResponse supplierDetailsResponse = mapSupplierDetails(supplier);
            allSupplierDetailsResponse.getSupplierDetails().add(supplierDetailsResponse.getSupplierDetails());
        }
        return allSupplierDetailsResponse;
    }
    @PayloadRoot(namespace = "http://webservicesexam/suppliers", localPart = "CreateItemDetailsRequest")
    @ResponsePayload
    public CreateSupplierDetailsResponse save(@RequestPayload CreateSupplierDetailsRequest request) {
        suppliersRepository.save(new Supplier(request.getSupplierDetails().getId(),
                request.getSupplierDetails().getNames(),
                request.getSupplierDetails().getEmail(),
                request.getSupplierDetails().getMobile()

        ));

        CreateSupplierDetailsResponse supplierDetailsResponse = new CreateSupplierDetailsResponse();
        supplierDetailsResponse.setSupplierDetails(request.getSupplierDetails());
        supplierDetailsResponse.setMessage("Created Successfully");
        return supplierDetailsResponse;
    }

    @PayloadRoot(namespace = "http://webservicesexam/items", localPart = "UpdateSupplierDetailsRequest")
    @ResponsePayload
    public UpdateSupplierDetailsResponse update(@RequestPayload UpdateSupplierDetailsRequest request) {
        UpdateSupplierDetailsResponse supplierDetailsResponse = null;
        Optional<Supplier> existingSupplier = this.suppliersRepository.findById((int) request.getSupplierDetails().getId());
        if(existingSupplier.isEmpty() || existingSupplier == null) {
            supplierDetailsResponse = mapSupplierDetail(null, "Id not found");
        }
        if(existingSupplier.isPresent()) {

            Supplier _supplier = existingSupplier.get();
            _supplier.setNames(request.getSupplierDetails().getNames());
            _supplier.setEmail(request.getSupplierDetails().getEmail());
            _supplier.setMobile(request.getSupplierDetails().getMobile());
            suppliersRepository.save(_supplier);
            supplierDetailsResponse = mapSupplierDetail(_supplier, "Updated successfully");

        }
        return supplierDetailsResponse;
    }

    @PayloadRoot(namespace = "http://webservicesexam/suppliers", localPart = "DeleteSupplierDetailsRequest")
    @ResponsePayload
    public DeleteSupplierDetailsResponse delete(@RequestPayload DeleteItemDetailsRequest request) {

        System.out.println("ID: "+request.getId());
        suppliersRepository.deleteById((int) request.getId());

        DeleteSupplierDetailsResponse supplierDetailsResponse = new DeleteSupplierDetailsResponse();
        supplierDetailsResponse.setMessage("Deleted Successfully");
        return supplierDetailsResponse;
    }

    private GetSupplierDetailsResponse mapSupplierDetails(Supplier supplier){
        SupplierDetails supplierDetails = mapSupplier(supplier);

        GetSupplierDetailsResponse supplierDetailsResponse = new GetSupplierDetailsResponse();

        supplierDetailsResponse.setSupplierDetails(supplierDetails);
        return supplierDetailsResponse;
    }

    private UpdateSupplierDetailsResponse mapSupplierDetail(Supplier supplier, String message) {
        SupplierDetails supplierDetails = mapSupplier(supplier);
        UpdateSupplierDetailsResponse supplierDetailsResponse = new UpdateSupplierDetailsResponse();

        supplierDetailsResponse.setSupplierDetails(supplierDetails);
        supplierDetailsResponse.setMessage(message);
        return supplierDetailsResponse;
    }
    private SupplierDetails mapSupplier(Supplier supplier){
        SupplierDetails supplierDetails = new SupplierDetails();
        supplierDetails.setEmail(supplier.getEmail());
        supplierDetails.setMobile(supplier.getMobile());
        supplierDetails.setNames(supplier.getNames());
        return supplierDetails;
    }
}
