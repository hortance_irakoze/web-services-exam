package com.example.webservicesexam.soap.repository;

import com.example.webservicesexam.soap.bean.Item;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ItemsRepository extends JpaRepository<Item,Integer> {

}
