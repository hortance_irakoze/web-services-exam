package com.example.webservicesexam.soap.repository;

import com.example.webservicesexam.soap.bean.Supplier;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ISuppliersRepository extends JpaRepository<Supplier,Integer> {
}
