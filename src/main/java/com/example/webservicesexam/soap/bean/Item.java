package com.example.webservicesexam.soap.bean;


import com.example.webservicesexam.soap.enums.Status;

import javax.persistence.*;

@Entity
//id, name, itemCode, status,price,supplier
public class Item {
    @Id
    @GeneratedValue
    private int id;
    private String name;
    private String itemCode;
    private Status status;
    private int price;
    @OneToOne
    private Supplier supplier;

    public Item() {
    }



    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getItemCode() {
        return itemCode;
    }

    public void setItemCode(String itemCode) {
        this.itemCode = itemCode;
    }

    public Status getStatus() {
        return status;
    }

    public void setStatus(Status status) {
        this.status = status;
    }

    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    public Supplier getSupplier() {
        return supplier;
    }

    public void setSupplier(Supplier supplier) {
        this.supplier = supplier;
    }

    public Item(long id, String itemCode, String name, int price, Status status, Supplier supplier) {
    }
}
