package com.example.webservicesexam;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class WebServicesExamApplication {

    public static void main(String[] args) {
        SpringApplication.run(WebServicesExamApplication.class, args);
    }

}
