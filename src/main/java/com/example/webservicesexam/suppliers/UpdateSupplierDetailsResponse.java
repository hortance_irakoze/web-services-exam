package com.example.webservicesexam.suppliers;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
        "supplierDetails",
        "message"
})
@XmlRootElement(name = "UpdateSupplierDetailsResponse")
public class UpdateSupplierDetailsResponse {
    @XmlElement(required = true)
    protected SupplierDetails supplierDetails;
    @XmlElement(required = true)
    protected String message;

    public SupplierDetails getSupplierDetails() {
        return supplierDetails;
    }
    public void setSupplierDetails(SupplierDetails value) {
        this.supplierDetails = value;
    }
    public String getMessage() {
        return message;
    }
    public void setMessage(String value) {
        this.message = value;
    }


}
