package com.example.webservicesexam.suppliers;
import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
        "supplierDetails"
})
@XmlRootElement(name = "GetAllSupplierDetailsResponse")
public class GetAllSupplierDetailsResponse {
    @XmlElement(required = true)
    protected List<SupplierDetails> supplierDetails;


    public List<SupplierDetails> getSupplierDetails() {
        if (supplierDetails == null) {
            supplierDetails = new ArrayList<SupplierDetails>();
        }
        return this.supplierDetails;
    }

}
