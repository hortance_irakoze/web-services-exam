package com.example.webservicesexam.suppliers;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
public class UpdateSupplierDetailsRequest {

    @XmlElement(required = true)
    protected SupplierDetails supplierDetails;

    public SupplierDetails getSupplierDetails() {
        return supplierDetails;
    }


    public void setSupplierDetails(SupplierDetails value) {
        this.supplierDetails = value;
    }
}
