package com.example.webservicesexam.suppliers;
import javax.xml.bind.annotation.XmlRegistry;

@XmlRegistry
public class ObjectFactory {

    public ObjectFactory() {
    }


    public GetAllSupplierDetailsResponse createGetAllSupplierDetailsResponse() {
        return new GetAllSupplierDetailsResponse();
    }

    public GetAllSupplierDetailsRequest createGetAllSupplierDetailsRequest() {
        return new GetAllSupplierDetailsRequest();
    }


    public CreateSupplierDetailsRequest createCreateSupplierDetailsRequest() {
        return new CreateSupplierDetailsRequest();
    }


    public CreateSupplierDetailsResponse createCreateSupplierDetailsResponse() {
        return new CreateSupplierDetailsResponse();
    }

    public DeleteSupplierDetailsRequest createDeleteSupplierDetailsRequest() {
        return new DeleteSupplierDetailsRequest();
    }

    public SupplierDetails createSupplierDetails() {
        return new SupplierDetails();
    }


    public UpdateSupplierDetailsRequest createUpdateSupplierDetailsRequest() {
        return new UpdateSupplierDetailsRequest();
    }


    public GetSupplierDetailsRequest createGetSupplierDetailsRequest() {
        return new GetSupplierDetailsRequest();
    }


    public GetSupplierDetailsResponse createGetSupplierDetailsResponse() {
        return new GetSupplierDetailsResponse();
    }


    public DeleteSupplierDetailsResponse createDeleteSupplierDetailsResponse() {
        return new DeleteSupplierDetailsResponse();
    }


    public UpdateSupplierDetailsResponse createUpdateSupplierDetailsResponse() {
        return new UpdateSupplierDetailsResponse();
    }

}

