package com.example.webservicesexam.suppliers;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
        "message"
})
@XmlRootElement(name = "DeleteSupplierDetailsResponse")
public class DeleteSupplierDetailsResponse {
    @XmlElement(required = true)
    protected String message;

    public String getMessage() {
        return message;
    }


    public void setMessage(String value) {
        this.message = value;
    }

}
