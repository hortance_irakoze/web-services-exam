package com.example.webservicesexam.suppliers;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
        "id"
})
@XmlRootElement(name = "DeleteSupplierDetailsRequest")
public class DeleteSupplierDetailsRequest {
    protected int id;

    public int getId() {
        return id;
    }


    public void setId(int value) {
        this.id = value;
    }

}
